define(

    function module( ) {

        'use strict' ;

        var Preload ;

        Preload = function Preload( source, success ) {

            var initialize ;

            initialize = function initialize( source, success ) {

                // define image
                this.image = new Image( ) ;

                // define user's success callback
                this.image.onload = success ;

                // define image source
                this.image.src = source ;

            // keep the context
            }.bind( this ) ;

            // initialize this instance
            initialize( source, success ) ;
        } ;

        return ( Preload ) ;
    }
) ;
